const assert = require('assert')
const { checkEnableTime } = require('../JobPosting')

describe('JobPosting', function () {

  it('should return false when เวลาสมัครหลังเวลาสิ้นสุด', function () {
    // Arrange
    const startTime = new Date(2021, 0, 31)
    const endTime = new Date(2021, 1, 5)
    const today = new Date(2021, 1, 6)
    const expectedResult = false
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)

    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })

  it('should return false when เวลาสมัครก่อนเวลาเริ่มต้น', function () {
    // Arrange
    const startTime = new Date(2021, 0, 31)
    const endTime = new Date(2021, 1, 5)
    const today = new Date(2021, 0, 30)
    const expectedResult = false
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)

    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })

  it('should return true when เวลาสมัครตรงกับเวลาเริ่มต้น', function () {
    // Arrange
    const startTime = new Date(2021, 0, 31)
    const endTime = new Date(2021, 1, 5)
    const today = new Date(2021, 0, 31)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)

    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })

  it('should return true when เวลาสมัครตรงกับเวลาสิ้นสุด', function () {
    // Arrange
    const startTime = new Date(2021, 0, 31)
    const endTime = new Date(2021, 1, 5)
    const today = new Date(2021, 1, 5)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)

    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })

  it('should return true when เวลาสมัครอยู่ระหว่างเริ่มต้นและสิ้นสุด', function () {
    // Arrange
    const startTime = new Date(2021, 0, 31)
    const endTime = new Date(2021, 1, 5)
    const today = new Date(2021, 1, 2)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)

    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
})

